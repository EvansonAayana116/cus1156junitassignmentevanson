package JunitPrac;

import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class MyPetTest {

	@BeforeEach
	void setUp() throws Exception {
		
		MyPet jen = new MyPet("Jennifer", 2, "Apple");
		MyPet ken = new MyPet ("Kenneth", 3, "granola bars");
	}

	@Test
	public void testIsHealthy() {//#5
		MyPet jen = new MyPet("Jennifer", 2, "Apple");
		MyPet ken = new MyPet ("Kenneth", 3, "granola bars");

		//fail("Not yet implemented");
		//assertSame("granola bars",jen.isHealthy());
		//assertEquals("granola bars",jen.isHealthy());
		//assertEquals("granola bars",ken.isHealthy());
		
		assertNotEquals("granola bars",jen.isHealthy());
		assertNotEquals("granola bars",ken.isHealthy());
	}

	@Test
	public void testFitsInCage() {//#6
		
		MyPet jen = new MyPet("Jennifer", 2, "Apple");
		MyPet ken = new MyPet ("Kenneth", 3, "granola bars");
		
		
		
		
		assertFalse(jen.fitsInCage(2)); //Smaller than 2
		assertFalse(ken.fitsInCage(3)); //Smaller than 2

		//Equals
		//assertSame("Test fails",2,jen.fitsInCage(2)); //Fails
		assertNotSame(2,jen.fitsInCage(2)); //Passes
		assertNotSame(3,ken.fitsInCage(2)); //Passes

		//Larger
		
		
		
		
	}//end of testFitsInCage

	@Test
	public void testLengthInInches() {
		MyPet jen = new MyPet("Jennifer", 2, "Apple");
		MyPet ken = new MyPet ("Kenneth", 3, "granola bars");

		
		
		assertSame(24,jen.lengthInInches(2)); //Passes
		assertSame(36,ken.lengthInInches(3)); //Passes

		
		//"length is in inches"
	}
	
	
}
