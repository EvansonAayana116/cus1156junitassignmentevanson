package JunitPrac;

public class MyPet {

	
	private String name;
	private int length; // the length of the bob, in feet
	private String favoriteFood;
	
	public MyPet (String name, int length, String favoriteFood){
		this.name = name;
		this.length = length;
		this.favoriteFood = favoriteFood;
	 }
	 
	 // returns true if this bob constructor is healthy
	
	 public boolean isHealthy(){
		 return this.favoriteFood.equals("granola bars");
	 }
	 
	 
	// returns true if the length of this boa constructor is
	
	 // less than the given cage length
	public boolean fitsInCage(int cageLength){
		return this.length < cageLength;
	 }
	
	// produces the length of the MyPet in inches 
	public int lengthInInches(int length){ // feet to inches
		
		return this.length*12 ;
	} 
	


} 